<?php 
/*
 * Product Q&A
 */

global $custom_fields_arr;
if(!$custom_fields_arr) return;

$q_a_arr = $custom_fields_arr['q_a_repeater'];

if( !is_array($q_a_arr) || count($q_a_arr) == 0)
    return;

$form_title = 'Product Q&A';

//make gravity form to notify Brand owner email about QA form submission
$cc_brand_owner_email = 0;
if ( $custom_fields_arr['brand'] ){
	$brand_id = $custom_fields_arr['brand']->ID;
	$brand_name = $custom_fields_arr['brand']->post_title;
	$email_product_qa_requests_to_brand_owner = get_post_meta($brand_id,'email_product_qa_requests_to_brand_owner',true);
	if($email_product_qa_requests_to_brand_owner == 1)
		$cc_brand_owner_email = $brand_id;
}
$gravyty_form_field_values = Array(
	'cc_brand_owner_email'=>$cc_brand_owner_email,
	'brand_name'=>$brand_name,
);

?>

<?php if(count($q_a_arr) > 0): ?>
	<div class="space p-t-30"></div>
	<h2 class="m-0 fs-16 lh-20 fs-xl-24 lh-xl-29 font-weight-600"><?php echo __("Community Questions"); ?></h2>

	<div class="row no-gutters q-a-container">
		<?php $i=0; ?>
		<?php foreach ($q_a_arr as $q_a_record): ?>
			<?php $i++; ?>
			<div class="col-12 q_a_header m-t-20 p-x-10 p-y-10 p-x-xl-20 p-y-xl-20 fs-14 lh-24 fs-xl-18 lh-xl-22" data-toggle="collapse" data-target="#q_a_<?php echo $i; ?>" aria-expanded="false" aria-controls="q_a_<?php echo $i; ?>">
				Q: <?php echo $q_a_record['question']; ?>
			</div>
			<div class="col-12 q_a_body collapse p-y-10 p-x-xl-20 p-y-xl-20 fs-12 lh-22 fs-xl-16 lh-xl-26" id="q_a_<?php echo $i; ?>">
				A: <?php echo $q_a_record['answer']; ?>
			</div>
			<div class="col-12 q_a_footer"  data-toggle="collapse" data-target="#q_a_<?php echo $i; ?>" aria-expanded="false" aria-controls="q_a_<?php echo $i; ?>"></div>
		<?php endforeach; ?>
	</div>

	<div class="row no-gutters justify-content-center">
		<button type="button" class="btn fs-14 lh-17 font-weight-600 ask-a-question btn-outline-primary p-y-13 p-x-50 m-y-40" data-toggle="modal" data-target="#qa_modal"><?php echo _("Ask a question"); ?></button>
	</div>


	<?php // Modal Form ?>
	<div class="modal" id="qa_modal" tabindex="-1" role="dialog" aria-labelledby="qa_modal_title" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered auth-complex-form" role="document">
			<div class="modal-content login-form">
				<div class="modal-header">
				<h2 class="m-x-15 m-y-15" id="qa_modal_title">Ask a Question</h2>
				<button type="button" class="close p-b-20 p-t-20 p-x-25" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				</div>
				<div class="modal-body p-t-35 p-b-35">
					<div class="m-auto col-12 col-md-10 p-x-5 p-y-5">
						<?php gravity_form( $form_title, false, false, false, $gravyty_form_field_values, true, 0 ); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>