import React from "react";
import { connect, cartActions, apiSettings } from "inbooma-api";
import Select from "react-select";
import _ from "lodash";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBan } from "@fortawesome/free-solid-svg-icons";

import { valueFromId } from "../../helpers/selectHelpers";

function BikesVariables(props) {
  const {
    contingents,
    cartItems,
    contingentNumbersAvailableForLocation
  } = props;

  const handleContingentNameFilterChange = option => {
    let contingent_amount = _.get(option, "value", null);
    let contingent_id = _.get(option, "contingent_id", null);
    let contingent = _.get(contingents, contingent_id, false);

    //remove items from cart
    _(cartItems)
      .filter({ contingent_id: contingent_id })
      .forEach(function(cart_item) {
        props.removeCartItemById({ id: cart_item._id });
      });

    //add items to cart
    let defaultPriceTariff = apiSettings.getSetting("defaultPriceTariff");
    let pricetariff_id = _.has(contingent.pricetariffs, defaultPriceTariff)
      ? defaultPriceTariff
      : _(contingent.pricetariffs)
          .keys()
          .first();
    for (let y = 1; y <= contingent_amount; y++) {
      let cart_item = {
        ...contingent,
        contingent_id,
        amount: 1,
        pricetariff_id
      };
      props.addCartItem(cart_item);
    }
  };

  const listItems = _(contingents)
    .map(function(item, i) {
      const size_tag = _(item.tags)
        .intersection(["XS", "S", "M", "L", "XL"])
        .join(", ");

      const items_in_cart = _(cartItems)
        .filter({ contingent_id: i })
        .value().length;

      // TODO app.config.minReservationLimits
      let min_reservation_limit = 1;
      let availables_cnt =
        parseInt(_.get(contingentNumbersAvailableForLocation, i, 0)) || 0;
      if (_.isEmpty(contingentNumbersAvailableForLocation))
        availables_cnt = null;

      let availables_array = [];
      if (availables_cnt === 0) {
        const labelWithIcon = <FontAwesomeIcon icon={faBan} />;
        availables_array.push({
          label: labelWithIcon,
          value: 0,
          contingent_id: i
        });
      }

      if (availables_cnt > 0)
        availables_array.push({ label: 0, value: 0, contingent_id: i });

      for (let x = min_reservation_limit; x <= availables_cnt; x++) {
        availables_array.push({ label: x, value: x, contingent_id: i });
      }

      const selectStyles = {
        control: styles => ({ ...styles, minWidth: 100 })
      };

      return (
        <div key={`${i}-${size_tag}`} className="col">
          {size_tag ? <label className="mb-0 mt-1">{size_tag}</label> : null}

          <Select
            styles={selectStyles}
            options={availables_array}
            value={valueFromId(availables_array, items_in_cart)}
            onChange={handleContingentNameFilterChange}
            placeholder=""
            isDisabled={availables_cnt === 0 ? true : false}
            isClearable={false}
            isLoading={_.isEmpty(contingentNumbersAvailableForLocation)}
          />
        </div>
      );
    })
    .value();

  return <div className="row no-gutters">{listItems}</div>;
}

const mapStateToProps = state => ({
  cart: state.cart_data.cart.data,
  cartItems: state.cart_data.cart_items.data,
  contingentNumbersAvailableForLocation:
    state.contingents_data.getContingentNumbersAvailableForLocation.data,
  prices: state.price_tariffs_data.getPrices
});

const mapDispatchToProps = dispatch => ({
  addCartItem: payload => dispatch(cartActions.addCartItem(payload)),
  removeCartItemById: payload =>
    dispatch(cartActions.removeCartItemById(payload))
});

export default connect(mapStateToProps, mapDispatchToProps)(BikesVariables);
